# piezometrie 0.0.3

* Modification des noms d'enregistrements des fichiers
* Partie BSH, ajout graphe et tableau Général: tous piezomètres.
* Modification de la nappe d'affectation pour la station de Gidy

# piezometrie 0.0.2

* Ajout de l'enregistrement en png de la carteREGION
* ajout fonction creatabhydogeol avec mise en compte des "pas de valeurs" (classe_niveau=0)
* ajout fonction creagraphehydrogeol avec mise en compte des "pas de valeurs" (classe_niveau=0)
* mise a jour de la fonction hydrogeolBSH

# piezometrie 0.0.1

* Mise en place des differentes fonctions CarteXXXX permettant de generer les cartes region, departement et aquifere.
* ajout fonction de connection a la base access
* Added a `NEWS.md` file to track changes to the package.
