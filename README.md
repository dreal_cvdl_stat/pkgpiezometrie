# Piézométrie

Travaux sur la piézométrie réalisés par

- Murielle Lethrosne

# pkgpiezometrie

*pkgpiezometrie* est un package qui permet la création des cartes piezo initialement produites par le SEBRINAL


## Installation à partir de gitlab dans RStudio :

``` r
remotes::install_gitlab("dreal_cvdl_stat/pkgpiezometrie")
```

## Exemple d'utilisation du package
``` r
library(pkgpiezometrie)
```

