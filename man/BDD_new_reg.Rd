% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/doc_BDD_new_reg.R
\docType{data}
\name{BDD_new_reg}
\alias{BDD_new_reg}
\title{BDD_new_reg
Base exemple de donnees du SEBRINAL
Recupere sur  "S:/07-SEBRINAL/08-Eau/R6-Piezo/RESEAU_PIEZO/carte_situation_hebdo/cartes_hebdo région_nouveau.MDB"
carte départements et aquifères}
\format{
An object of class \code{data.frame} with 160 rows and 25 columns.
}
\usage{
BDD_new_reg
}
\description{
BDD_new_reg
Base exemple de donnees du SEBRINAL
Recupere sur  "S:/07-SEBRINAL/08-Eau/R6-Piezo/RESEAU_PIEZO/carte_situation_hebdo/cartes_hebdo région_nouveau.MDB"
carte départements et aquifères
}
\keyword{datasets}
