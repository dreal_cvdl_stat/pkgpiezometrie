#' extrairelegend
#' @description extrairelegend() permet d extraire la legende dun graphe realise avec ggplot(). La legende peut ainsi etre utilisee separemment du graphe
#' @param myggplot un graphique ggplot
#'
#' @return gtable
#' @importFrom ggplot2 ggplot_gtable ggplot_build
#' @export
#' @examples
#' library(ggplot2)
#' gg<-ggplot(mtcars, aes(x=wt, y=mpg)) +
#' geom_point(aes(size=qsec))
#' extrairelegend(gg)

extrairelegend<-function(myggplot){
  tmp <- ggplot2::ggplot_gtable(ggplot2::ggplot_build(myggplot))
  leg <- which(sapply(tmp$grobs, function(x) x$name) == "guide-box")
  legend <- tmp$grobs[[leg]]
  return(legend)
}
