#' graphepiezocommun
#'
#' Permet la constrution de la carte commune pour les indicateurs piezo qui utilise la variable tendance_mensuelle (REG et DEP)
#'
#' @importFrom ggplot2 ggplot geom_sf geom_label geom_point element_blank coord_sf geom_text labs scale_fill_manual guide_legend scale_color_manual
#' @importFrom forcats fct_drop
#' @importFrom relayer rename_geom_aes
#' @importFrom ggspatial annotation_scale annotation_north_arrow
#' @importFrom stringr str_detect
#' @importFrom gouvdown theme_gouv_map theme_gouv
#' @importFrom shadowtext geom_shadowtext
#' @param couleur_couche_dep couleur pour les limites departementales;par defaut="black"
#' @param couche_commune boolean TRUE si on veut le conteur des principales communes; par defaut=FALSE
#' @param data dataframe donnees BDD piezo
#' @param size_point numerique taille des points des indicateurs piezo; par defaut=7
#' @param label_site vecteur des intitules des stations
#' @param vjust_site numerique, ajustement vertical des libelles des stations; par defaut=0.5
#' @param fontfacelabel character, typographie des libelles des stations; ex: "plain","italic","bold"; par defaut="plain"
#' @param size_label numerique; taille de police pour les libelles; par defaut=2
#' @param superpositionlabel boolean; specifie si on accepte la superposition des labels; par defaut=FALSE
#' @param zoom permet d'effectuer le zoom sur un aquifere en le citant "Calcaires","Jurassique","Cenomanien","Craie". par defaut=NA pour la region
#'
#' @return ggplot
#' @export
#'
#' @examples
graphepiezocommun <- function(couleur_couche_dep = "black",
                              couche_commune, data,
                              size_point = 7, label_site, vjust_site = 0.5,
                              fontfacelabel = "plain", size_label = 2, superpositionlabel = FALSE, zoom = NA) {
  
  g0 <- ggplot() +
    gouvdown::theme_gouv_map(plot_title_size = 14)+
    #############
    # GESTION DES COUCHES geologique pour le cenomanien
    {
      if (zoom %in% "C\u00e9nomanien") geom_sf(data = piezometrie::Afl_F7A, aes(fill = "A"))
    } +
    {
      if (zoom %in% "C\u00e9nomanien") geom_sf(data = piezometrie::CaptifF7A, aes(fill = "B"), alpha = 0.4)
    } +
    {
      if (zoom %in% "C\u00e9nomanien") {
        scale_fill_manual(
          values = c("A" = "#7CCD7C", "B" = "#B4EEB4"),
          labels = c("nappe libre", "nappe captive sous recouvrement"),
          name = "Contexte hydrog\u00e9ologique",
          guide = guide_legend(override.aes = list(alpha = c(NA, 0.4)))
        )
      }
    } +
    #############
    # GESTION DES COUCHES geologique pour la craie
    {
      if (zoom %in% "Craie") geom_sf(data = piezometrie::Affl_craie, aes(fill = "A"))
    } +
    {
      if (zoom %in% "Craie") geom_sf(data = piezometrie::Captif_craie, aes(fill = "B"), alpha = 0.2)
    } +
    {
      if (zoom %in% "Craie") {
        scale_fill_manual(
          values = c("A" = "#e2f2a1", "B" = "#e2f2a1"),
          labels = c("nappe libre", "nappe captive sous recouvrement"),
          name = "Contexte hydrog\u00e9ologique",
          guide = guide_legend(override.aes = list(alpha = c(NA, 0.2)))
        )
      }
    } +
    #############
    # GESTION DES COUCHES geologique pour les calcaires de beauce
    {
      if (zoom %in% "Calcaires") geom_sf(data = piezometrie::extension_calcaire %>% filter(.data$Label == "Calcaires (Tertiaire)"), aes(fill = "A"), alpha = 0.4)
    } +
    {
      if (zoom %in% "Calcaires") geom_sf(data = piezometrie::extension_calcaire %>% filter(.data$Label == ""),
                                         aes(fill = "B"), alpha = 0.7)
    } +
    {
      if (zoom %in% "Calcaires") {
        scale_fill_manual(
          values = c("A" = "lightgoldenrod2", "B" = "#FFF68F"),
          labels = c("calcaires \u00e0 l'affleurement", "calcaires sous recouvrement"),
          name = "Contexte g\u00e9ologique",
          guide = guide_legend(override.aes = list(alpha = c(0.4, 0.7)))
        )
      }
    } +
    #############
    # GESTION DES COUCHES geologique pour le jurassique
    {
      if (zoom %in% "Jurassique") geom_sf(data = piezometrie::Affleurement_F10A, aes(fill = "A"), alpha = 0.4)
    } +
    {
      if (zoom %in% "Jurassique") geom_sf(data = piezometrie::Affl_F11A, aes(fill = "B"), alpha = 0.7)
    } +
    {
      if (zoom %in% "Jurassique") geom_sf(data = piezometrie::Affl_Lias, aes(fill = "C"))
    } +
    {if(zoom %in% "Jurassique")scale_fill_manual(values = c("A" = "#BFEFFF","B" = "#3D8E99","C"="#D8BFD8"),
                                            labels = c("Malm", "Dogger","Lias"),
                                            name = "Contexte g\u00e9ologique",
                                            guide = guide_legend(override.aes = list(alpha=c(0.4,0.7,1))))}+
    #############
    # GESTION DES COUCHES d habillage standart a toutes les cartes: DEP et hydro
    geom_sf(data = piezometrie::dep, size = 0.5, fill = NA, col = couleur_couche_dep) +
    geom_sf(data = piezometrie::hydro, size = 0.3, fill = NA, col = "deepskyblue") +
    #############
    # GESTION de la COUCHE commune si l argument couche_commune=TRUE
    {
      if (couche_commune) geom_sf(data = piezometrie::commune, fill = "grey")
    } +
    #############
    # Affichage des points sur la carte pour les stations ayant une tendance 
    geom_point(
      data = data %>%
        dplyr::filter(.data$shp != 0)%>%
        dplyr::mutate(shp = fct_drop(.data$shp, only = "0")) %>%
        dplyr::mutate(classe_niveau = fct_drop(.data$classe_niveau, only = "0")),
      aes(
        x = .data$X, y = .data$Y, colour = .data$classe_niveau, fill2 = .data$classe_niveau,
        shape = .data$shp
      ), size = size_point
    ) %>% relayer::rename_geom_aes(new_aes = c("fill" = "fill2"))+
    #############
    # Affichage des points sur la carte pour les stations sans tendance a afficher
    geom_point(data = data %>% dplyr::filter(.data$shp == 0), aes(x = .data$X, y = .data$Y), size = 2, pch = 1) +
    #############
    # Gestion des libelles 
  { if (zoom %in% "Jurassique")   
    shadowtext::geom_shadowtext(
      data = data %>% dplyr::filter(.data$new_Aquifere=="Malm"),
      aes(x = .data$X, y = .data$Y, label = .data$lab_site),
      size = size_label, col="#BFEFFF",bg.colour="black",
      check_overlap = superpositionlabel, fontface = fontfacelabel,
      nudge_y=pull(data %>% dplyr::filter(.data$new_Aquifere=="Malm"),.data$Nom_du_site_ajust)*1500,
      family="Marianne" ) 
  }+
    { if (zoom %in% "Jurassique")   
      shadowtext::geom_shadowtext(
        data = data %>% dplyr::filter(.data$new_Aquifere=="Dogger"),
        aes(x = .data$X, y = .data$Y, label = .data$lab_site),
        size = size_label, col="#3D8E99",bg.colour="black",
        check_overlap = superpositionlabel, fontface = fontfacelabel,
        nudge_y=pull(data %>% dplyr::filter(.data$new_Aquifere=="Dogger"),.data$Nom_du_site_ajust)*1500,
        family="Marianne"
      )
    }+
    { if (zoom %in% "Jurassique")   
      shadowtext::geom_shadowtext(
        data = data %>% dplyr::filter(.data$new_Aquifere=="Lias"),
        aes(x = .data$X, y = .data$Y, label = .data$lab_site),
        size = size_label, col="#D8BFD8",bg.colour="black",
        check_overlap = superpositionlabel, fontface = fontfacelabel,
        nudge_y=pull(data %>% dplyr::filter(.data$new_Aquifere=="Lias"),.data$Nom_du_site_ajust)*1500,
        family="Marianne"
      )
    }+
  {
     if (zoom != "Jurassique" | is.na(zoom))
    geom_text(
      data = data, aes(x = .data$X, y = .data$Y, label = .data$lab_site),size = size_label,
      check_overlap = superpositionlabel, fontface = fontfacelabel, 
      nudge_y=vjust_site*1500,family="Marianne"
        ) 
   }+
    #############
    # GESTION DES zoom
    {
      if (zoom %in% "C\u00e9nomanien") coord_sf(xlim = c(440106.4, 673284.8), ylim = c(6619100.9, 6848526.4), datum = NA)
    } +
    {
      if (zoom %in% "Craie") coord_sf(xlim = c(476829.3, 714174.1), ylim = c(6633687.6, 6870950.2), datum = NA)
    } +
    {
      if (zoom %in% "Calcaires") coord_sf(xlim = c(547090.0, 682073.5), ylim = c(6691070.8, 6832359.7), datum = NA)
    } +
    {
      if (zoom %in% "Jurassique") coord_sf(xlim = c(485000, 709599.9), ylim = c(6599100.9, 6822271.0), datum = NA)
    } +
    {
      if (zoom %in% "18") coord_sf(xlim = c(607045.5, 706063.7), ylim = c(6591419.5, 6725654.9), datum = NA)
    } +
    {
      if (zoom %in% "28") coord_sf(xlim = c(533590.0, 625417.5), ylim = c(6762748.6, 6872271.0), datum = NA)
    } +
    {
      if (zoom %in% "36") coord_sf(xlim = c(537216.2, 638987.4), ylim = c(6584220.1, 6687272.2), datum = NA)
    } +
    {
      if (zoom %in% "37") coord_sf(xlim = c(476864.7, 576133.0), ylim = c(6628313.2, 6737032.2), datum = NA)
    } +
    {
      if (zoom %in% "41") coord_sf(xlim = c(518603.6, 643359.9), ylim = c(6677576.5, 6783277.1), datum = NA)
    } +
    {
      if (zoom %in% "45") coord_sf(xlim = c(589119.1, 709599.9), ylim = c(6709232.0, 6805254.4), datum = NA)
    } +
    {
      if (is.na(zoom)) coord_sf(datum = NA,expand=FALSE)
    } +
    theme(
      rect = element_blank(),
      axis.text.x = element_blank(),
      axis.text.y = element_blank(),
      axis.ticks = element_blank()
    ) +
    labs(x = "", y = "")+
    ggspatial::annotation_scale(location = "br", line_width = .1,pad_x = unit(0.25, "in"))+
    ggspatial::annotation_north_arrow(location = "br", which_north = "true",
                                      height = unit(0.7, "cm"), width = unit(0.7, "cm"),
                                      pad_x = unit(1, "cm"), pad_y = unit(1, "cm"))

  return(g0)
}
