#' modifcoordonnees
#' modifcoordonnees permet la modification des coordonnees X et Y en fonction des erreurs de la base de donnees, mais aussi deplace les stations pour celle qui se superposent: ATHEE SUR CHER et SERIS
#' Attention, les noms des colonnes doivent etre sans accent (cf exemple)
#' @param tab dataframe comprenant les colonnes Commune, Aquifere, Nom_du_site et N__BSS
#'
#' @return dataframe
#' @importFrom dplyr mutate 
#' @importFrom stringr str_detect
#' @export
#'
#' @examples
#'  library(dplyr)
#'  mytab<-piezometrie::BDD_new_reg
#'  names(mytab)=iconv(names(mytab),to="ASCII//TRANSLIT")
#'  mytab<-mytab %>% 
#'  modifcoordonnees()

modifcoordonnees<-function(tab){
  res<-tab %>% 
    dplyr::mutate(X=case_when(.data$Nom_du_site == "Goimpy" | stringr::str_detect(.data$N__BSS,"02558X0034") ~ 607865,
                              (.data$Commune == "ATHEE-SUR-CHER" & .data$Aquifere == "C\u00e9nomanien") | 
                                stringr::str_detect(.data$N__BSS,"04883X0077") ~ 542454,
                              (.data$Commune == "ATHEE-SUR-CHER" & .data$Aquifere == "Craie S\u00e9no-Turonienne") | 
                                stringr::str_detect(.data$N__BSS,"04883X0081") ~ 545494,
                              (.data$Commune == "SERIS" & .data$Aquifere == "Calcaires de Beauce") | 
                                stringr::str_detect(.data$N__BSS,"03975X0002") ~ 586680,
                              (.data$Commune == "SERIS" & .data$Aquifere == "Craie S\u00e9no-Turonienne") | 
                                stringr::str_detect(.data$N__BSS,"03975X0076") ~ 590043,
                              TRUE~.data$X),
                  Y=case_when(.data$Nom_du_site == "Goimpy" | stringr::str_detect(.data$N__BSS,"02558X0034") ~ 6814324,
                              (.data$Commune == "ATHEE-SUR-CHER" & .data$Aquifere == "C\u00e9nomanien") | 
                                stringr::str_detect(.data$N__BSS,"04883X0077") ~ 6693780,
                              (.data$Commune == "ATHEE-SUR-CHER" & .data$Aquifere == "Craie S\u00e9no-Turonienne") | 
                                stringr::str_detect(.data$N__BSS,"04883X0081") ~ 6693820,
                              (.data$Commune == "SERIS" & .data$Aquifere == "Calcaires de Beauce") | 
                                stringr::str_detect(.data$N__BSS,"03975X0002") ~ 6740761,
                              (.data$Commune == "SERIS" & .data$Aquifere == "Craie S\u00e9no-Turonienne") | 
                                stringr::str_detect(.data$N__BSS,"03975X0076") ~ 6740734,
                              TRUE~.data$Y))
  return(res)
}