#' creagraphehydrogeolBSH
#'
#' @param mytab un data frame au format long comportant 3 colonnes: date (format dd/mm/yyyy), name (pour les catégories de répartition) et value (le nombre de piezo par catégorie)
#'
#' @return ggplot
#' @importFrom dplyr pull
#' @importFrom forcats fct_rev
#' @importFrom ggplot2 ggplot geom_bar element_text scale_fill_manual scale_color_manual scale_y_continuous scale_x_datetime labs theme
#' @importFrom gouvdown theme_gouv
#' @importFrom lubridate dmy
#' @importFrom scales percent
#' 
#' @export
#'
#' @examples

creagraphehydrogeolBSH<-function(mytab){
  
  mytab<-mytab %>% 
    mutate(date=as.POSIXct(lubridate::dmy(.data$date)))
  
  ggplot2::ggplot(data = mytab) +
    ggplot2::geom_bar(stat = "identity",position="fill",width=15e5,
             aes(x = .data$date, y = .data$value,fill =  forcats::fct_rev(.data$name), 
                 color = forcats::fct_rev(.data$name))) +
    ggplot2::scale_fill_manual(name="",values = c("grey","#0F60D0", "#87E8F6", "#16C361",
                                         "#F3F315","#FF9900", "#E31A1C")) +
    ggplot2::scale_color_manual(name="",values = c(rep("black",7)))+
    ggplot2::scale_y_continuous(expand = c(0,0),labels = scales::percent, breaks = seq(0, 1, 0.1)) +
    ggplot2::scale_x_datetime(expand = c(0,0),
                              breaks=unique(dplyr::pull(mytab,.data$date)),
                     date_labels = "%b-%y")+
    ggplot2::labs(title = "\u00c9volution mensuelle de la r\u00e9partition par classe des niveaux piezom\u00e8triques",
         x = "", y = "",
         caption = "") +
    gouvdown::theme_gouv()+
    ggplot2::theme(plot.title = ggplot2::element_text(size=16),
                   axis.text.x = ggplot2::element_text(size=9,vjust=-5))
}