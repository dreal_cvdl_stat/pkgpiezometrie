#' stat_etiquette_DEP_AQUIFERE
#' Table de correspondance pour les DEP et AQUIFERE pour la simplification du nom des sites et ajustement des etiquettes
#' Nom_du_site: nom tel qu il est present dans la table de donnees; data
#' new_etiquette: nom tel qu il doit apparaitre dans les cartes
#' @docType data
"stat_etiquette_DEP_AQUIFERE"
