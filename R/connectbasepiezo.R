#' connectbasepiezo
#' connectbasepiezo permet de se connecter à la base access et rapatrier les donnees dans la session RStudio
#' @return dataframe
#' @importFrom RODBC odbcDriverConnect sqlFetch
#' @importFrom dplyr full_join
#' @export
#'
#' @examples
#' print("Exemple a faire lorsqu on est connecte au reseau")
#' # connectbasepiezo()

connectbasepiezo<-function(){


con<-RODBC::odbcDriverConnect("Driver={Microsoft Access Driver (*.mdb, *.accdb)};
                         DBQ=//10.45.188.32/dreal/07-SEBRINAL/08-Eau/R6-Piezo/RESEAU_PIEZO/carte_situation_hebdo/indicateur_hebdo/courbes_ref_hebdo.MDB")

# assign('BDD_courbes_piezo',RODBC::sqlFetch(con, "LISTE_PIEZOS"),envir=parent.frame())
# assign('BDD_courbes_situation',RODBC::sqlFetch(con, "EXPORT_SITUATION"),envir=parent.frame())
# assign('BDD_courbes_piezo_param',RODBC::sqlFetch(con, "PARAMETRES"),envir=parent.frame())
# assign('BDD_courbes_paramstat',RODBC::sqlFetch(con, "PARAMETRES_STATISTIQUES"),envir=parent.frame())
# assign('BDD_courbes',BDD_courbes_piezo %>% dplyr::full_join(BDD_courbes_situation),envir=parent.frame())
# 
.GlobalEnv$BDD_courbes_piezo_param<-RODBC::sqlFetch(con, "PARAMETRES")
.GlobalEnv$BDD_courbes_piezo<-RODBC::sqlFetch(con, "LISTE_PIEZOS")
.GlobalEnv$BDD_courbes_situation<-RODBC::sqlFetch(con, "EXPORT_SITUATION")
.GlobalEnv$BDD_courbes<-.GlobalEnv$BDD_courbes_piezo %>% dplyr::full_join(.GlobalEnv$BDD_courbes_situation)

print("Les dataframes BDD_new_reg_XXX sont issues de: //10.45.188.32/dreal/07-SEBRINAL/08-Eau/R6-Piezo/RESEAU_PIEZO/carte_situation_hebdo/indicateur_hebdo/courbes_ref_hebdo.MDB")

con<-RODBC::odbcDriverConnect("Driver={Microsoft Access Driver (*.mdb, *.accdb)};
                         DBQ=//10.45.188.32/dreal/07-SEBRINAL/08-Eau/R6-Piezo/RESEAU_PIEZO/carte_situation_hebdo/cartes_hebdo/r\u00e9gion_nouveau.MDB")

# 
# assign('BDD_new_reg_piezo',RODBC::sqlFetch(con, "LISTE_PIEZOS"),envir=parent.frame())
# assign('BDD_new_reg_situation',RODBC::sqlFetch(con, "EXPORT_SITUATION"),envir=parent.frame())
# assign('BDD_new_reg_piezo_param',RODBC::sqlFetch(con, "PARAMETRES"),envir=parent.frame())
# assign('BDD_new_reg_paramstat',RODBC::sqlFetch(con, "PARAMETRES_STATISTIQUES"),envir=parent.frame())
# assign('BDD_new_reg',BDD_new_reg_piezo %>% dplyr::full_join(BDD_new_reg_situation),envir=parent.frame())

.GlobalEnv$BDD_new_reg_piezo_param<-RODBC::sqlFetch(con, "PARAMETRES")
.GlobalEnv$BDD_new_reg_piezo<-RODBC::sqlFetch(con, "LISTE_PIEZOS")
.GlobalEnv$BDD_new_reg_situation<-RODBC::sqlFetch(con, "EXPORT_SITUATION")
.GlobalEnv$BDD_new_reg<-.GlobalEnv$BDD_new_reg_piezo %>% dplyr::full_join(.GlobalEnv$BDD_new_reg_situation)


print("Les dataframes BDD_courbes_XXX sont issues de: //10.45.188.32/dreal/07-SEBRINAL/08-Eau/R6-Piezo/RESEAU_PIEZO/carte_situation_hebdo/cartes_hebdo/r\u00e9gion_nouveau.MDB")

}