#' hydrogeolBSH
#' permet la production des tableaux et des graphes comprenant les valeurs pour l hydrogeol du BSH 
#' @param data dataframe le tableau de donnees
#' @param chemin_enregistrement le chemin d enregistrement. attention les separateurs doit etre inverse slash vs antislash
#' @param chemin_historique le chemin d'accès (nom fichier inclus) du fichier historique des resultats piezo BSH. Il s'incremente a chaque lancement
#' @param date_bsh date de la donnee à completer
#' @return dataframe
#' 
#' @importFrom dplyr filter select rename group_by summarise across slice case_when
#' @importFrom tidyr pivot_longer
#' @importFrom forcats fct_relevel
#' @importFrom ggplot2 ggsave
#' @importFrom glue glue
#' @importFrom grDevices png dev.off cairo_pdf
#' @importFrom grid textGrob gpar unit unit.c grobHeight
#' @importFrom gridExtra ttheme_default tableGrob grid.arrange
#' @importFrom lubridate dmy today
#' @importFrom readxl read_xlsx
#' @importFrom stringr str_sub str_replace_all str_split
#' @importFrom tcltk tk_messageBox
#' @importFrom tidyselect everything vars_select_helpers
#' @importFrom xlsx2dfs xlsx2dfs dfs2xlsx
#' @importFrom stats na.omit
#' @importFrom gt gt tab_header cols_width tab_source_note html gtsave
#' @importFrom rlang eval_tidy enquo
#' @export
#' @examples


hydrogeolBSH<-function(data,chemin_enregistrement,chemin_historique,date_bsh){
  
  C<-piezometrie::creatabhydrogeolBSH(data,chemin_enregistrement)
  
  
  ddate<-glue::glue("01",
                    stringr::str_sub(date_bsh,3))
  print(.data$ddate)

  dfs <- xlsx2dfs::xlsx2dfs(chemin_historique,sep.names = " ",
                            rowNames=FALSE) # all sheets of file as list of dfs
  
  for (i in unique(pull(C,.data$Aquifere))){
    
    print(i)
    
    #recuperation de la bonne feuille du table
    t<-dfs[[i]] %>% 
      dplyr::mutate(date=format(lubridate::dmy(.data$date),"%d/%m/%Y")) #%>% 
      #stats::na.omit()
    
    temp<-C %>%
      dplyr::filter(.data$Aquifere==i)
    
    
    # #### MISE EN PAGE TABLEAU 
    # largeur_col=dplyr::case_when(i=="Jurassique"~"250px",
    #                              i=="Calcaires de Beauce"~"250px",
    #                              i=="Craie"~"30px",
    #                              i=="C\u00e9nomanien"~"30px")
    temp2<-temp %>% 
      dplyr::select(-.data$Aquifere)%>%
      gt::gt() %>% 
      gt::tab_header(
        title = "R\u00e9partition par classe"
      )
    if( i %in% c("Jurassique","Calcaires de Beauce")){
      temp2<-temp2 %>% gt::cols_width(1~gt::px(250))}
    
    if( i %in% c("Craie","C\u00e9nomanien")){
      temp2<-temp2 %>% gt::cols_width(1~gt::px(30))}
    
    temp2 %>% 
      gt::tab_source_note(
        source_note = gt::html("Avec DS : d\u00e9cennale s\u00e8che, QS : quinquennale s\u00e8che, QH : quinquennale humide et DH : d\u00e9cennale humide (cf. glossaire en fin de bulletin).
           <br>R\u00e9alisation : \u00a9DREAL Centre-Val de Loire"))%>%
      gt::gtsave(paste0(chemin_enregistrement,"/", stringr::str_replace_all(lubridate::today(), "-", "_"),
                        "_Hydro_BSH_TABLEAU_", stringr::str_split(i, " ", simplify = TRUE)[[1]],".png"),
                 expand = 10)
    
    #### MISE EN PAGE GRAPHE   

    # VERIFIE SI LES DONNEES ONT DEJA ETE INTEGREE DANS LE FICHIER HISTORIQUE
    reponse="yes"
    
    if (ddate %in% pull(t,date)){
      
      reponse<-tcltk::tk_messageBox(caption = "ATTENTION",
                                    message = paste("Les donn\u00e9es piezo",i,"pour",
                                                    format(lubridate::dmy(ddate),"%b-%y"),"sont d\u00e9j\u00e0 dans le fichier.
                                       \n Voulez vous les \u00e9craser?"), 
                                    icon = "info", type = "yesno")}
    
    if(reponse=="yes"){
      
      t2<-t %>% 
        dplyr::filter(date!=ddate) %>% #supprime la donnee du mois en question si elle est presente
        dplyr::bind_rows(temp %>% 
                           dplyr::group_by(.data$Aquifere) %>% 
                           dplyr::summarise(dplyr::across(tidyselect::vars_select_helpers$where(is.numeric), ~ sum(.x, na.rm = TRUE)))%>% 
                           dplyr::mutate(date=format(lubridate::dmy(ddate),"%d/%m/%Y")) %>% 
                           dplyr::select(.data$date,tidyselect::everything(),
                                         -.data$Aquifere,-.data[["Nombre de piezom\u00e8tres"]])) 
      
      
      # remplace la table de la feuille i par la table completee de la derniere donnee
      dfs[[i]] <- as.data.frame(t2)  
      
    }
    # si on ne souhaite pas ecraser les donnees, les graphes qui suivent seront fait a partir des donnees deja existantes dans le fichier
    if(reponse=="no"){ t2<-t}
    
    #graphe sur les 13 derniers mois
    t3<-t2 %>% 
      dplyr::slice((nrow(t2)-12):nrow(t2))
    
    t_pivot<-tidyr::pivot_longer(t3,-date) %>% 
      dplyr::mutate(name=forcats::fct_relevel(.data$name,"Inf\u00e9rieur au DS","Entre DS et QS","Entre QS et moyenne",
                                       "Entre moyenne et QH", "Entre QH et DH",
                                       "Sup\u00e9rieur au DH","Pas de valeurs"))
    
    gg<-piezometrie::creagraphehydrogeolBSH(t_pivot)
    
    ggplot2::ggsave(paste0(chemin_enregistrement,"/", stringr::str_replace_all(lubridate::today(), "-", "_"), "_Hydro_BSH_GRAPHE_", stringr::str_split(i, " ", simplify = TRUE)[[1]],".pdf"),
                    width = 29.7, height = 17, units = "cm",dpi=300,device=cairo_pdf)
    
  }
  
  #enregistre le tableur avec toutes les feuilles maj
  xlsx2dfs::dfs2xlsx(dfs, chemin_historique,rowNames=FALSE)
}