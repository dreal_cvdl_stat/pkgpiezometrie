#' ajustementlibellestation
#' Cette fonction permet d ajuster la disposition des libelles des stations en fonction du symbole de tendance.
#' Il se base sur une variable presente dans les fichiers stat_etiquette_XXXX, qui possede une variable vjust_xxx valant 1 si l on souhaite l etiquette au dessus ou -1 sinon on souhaite l etiquette en dessous.
#' @param data data.frame sur les piezo comprenant à minima une colonne vjust_xxx
#' @param zone pour la region: REG; pour les departements: "18","28","36","37","41","45"; pour les aquiferes: "Calcaires", "Craie", "Cenomanien", "Jurassique"
#' @importFrom magrittr %>%
#' @importFrom dplyr mutate
#' @importFrom stringr str_count
#' @return data.frame
#' @export
#'
#' @examples
ajustementlibellestation <- function(data,zone) {
  
  if (zone=="REG"){
    data<-data %>%     dplyr::mutate(vjust1=case_when(shp=="1"~(.data$vjust*6),
                                                      shp=="3"~(1+.data$vjust*6),
                                                      shp=="2"~(.data$vjust*5),
                                                      shp=="0"~(.data$vjust*2))) %>%  
      dplyr::mutate(vjust=.data$vjust1+ 1.5*.data$vjust*stringr::str_count(.data$New_etiquette, pattern = "_"),
                    Nom_du_site_ajust =  .data$vjust)
  }

  ###################################################
  
  if (zone==18){
  data<-data %>% dplyr::mutate(vjust=case_when(shp=="1"~(.data$vjust_dep*2.5),
                                 shp=="3"~(.data$vjust_dep*3+0.5),
                                 shp=="2"~(.data$vjust_dep*2.5),
                                 shp=="0"~(.data$vjust_dep*2))) 
}

if (zone==28) {
  data<-data %>% dplyr::mutate(vjust=case_when(shp=="1"~(.data$vjust_dep*2.5-0.2),
                                                shp=="3"~(.data$vjust_dep*2.5),
                                                shp=="2"~(.data$vjust_dep*2.3-0.2),
                                                shp=="0"~(.data$vjust_dep*1.75))) 
}
  
  if (zone==36){
    data<-data %>% dplyr::mutate(vjust=case_when(shp=="1"~(.data$vjust_dep*2.5-0.3),
                                                  shp=="3"~(.data$vjust_dep*2.5+0.5),
                                                  shp=="2"~(.data$vjust_dep*2.5),
                                                  shp=="0"~(.data$vjust_dep*2))) 
  }
  
  if (zone==37){
    data<-data %>% dplyr::mutate(vjust=case_when(shp=="1"~(.data$vjust_dep*2.5-0.3),
                                                  shp=="3"~(.data$vjust_dep*2.5+0.5),
                                                  shp=="2"~(.data$vjust_dep*2.5),
                                                  shp=="0"~(.data$vjust_dep*2))) 
  }
  if (zone==41){
    data<-data %>% dplyr::mutate(vjust=case_when(shp=="1"~(.data$vjust_dep*2.5-0.3),
                                                  shp=="3"~(.data$vjust_dep*2.5+0.5),
                                                  shp=="2"~(.data$vjust_dep*2.5),
                                                  shp=="0"~(.data$vjust_dep*2)))
  }
  
  if (zone==45){
    data<-data %>% dplyr::mutate(vjust=case_when(shp=="1"~(.data$vjust_dep*2.5-0.3),
                                                 shp=="3"~(.data$vjust_dep*2.5+0.5),
                                                 shp=="2"~(.data$vjust_dep*2.5),
                                                 shp=="0"~(.data$vjust_dep*2)))
  }
  
  if (zone %in% c("18","28", "36", "37", "41", "45")){
  data<-data %>% 
    dplyr::mutate(vjust=ifelse(stringr::str_count(.data$Nom_du_site2, pattern = "\n")==2,
                               .data$vjust*1.3,.data$vjust))
  }
  
###################################################
  
  if (zone=="Calcaires"){
    data<-data %>% dplyr::mutate(vjust=case_when(shp=="1"~(.data$vjust_aquifere*2.5-0.3),
                                                 shp=="3"~(.data$vjust_aquifere*2+0.5),
                                                 shp=="2"~(.data$vjust_aquifere*2),
                                                 shp=="0"~(.data$vjust_aquifere*1.5)))
  }
  if (zone=="Craie"){
    data<-data %>% dplyr::mutate(vjust=case_when(shp=="1"~(.data$vjust_aquifere*2.8-0.3),
                                                 shp=="3"~(.data$vjust_aquifere*2.8+0.5),
                                                 shp=="2"~(.data$vjust_aquifere*2.6),
                                                 shp=="0"~(.data$vjust_aquifere*1.5)))
  }
  
  if (zone=="C\u00e9nomanien"){
    data<-data %>% dplyr::mutate(vjust=case_when(shp=="1"~(.data$vjust_aquifere*3.2-0.4),
                                                 shp=="3"~(.data$vjust_aquifere*2.8+0.6),
                                                 shp=="2"~(.data$vjust_aquifere*2.6),
                                                 shp=="0"~(.data$vjust_aquifere*1.5)))
  }
  if (zone=="Jurassique"){
    data<-data %>% dplyr::mutate(vjust=case_when(shp=="1"~(.data$vjust_aquifere*3-0.4),
                                                 shp=="3"~(.data$vjust_aquifere*2.9+0.6),
                                                 shp=="2"~(.data$vjust_aquifere*2.6),
                                                 shp=="0"~(.data$vjust_aquifere*1.5)))
  } 
  
  
  if (zone %in% c("Calcaires", "Craie", "C\u00e9nomanien", "Jurassique")){
    data<-data %>% 
      dplyr::mutate(vjust=ifelse(stringr::str_count(.data$lab_site, pattern = "\n")==1,
                                 .data$vjust*1.3,.data$vjust))
  }
  
  
  data<-data %>% dplyr::mutate(Nom_du_site_ajust =  .data$vjust)
  
return(data)
}